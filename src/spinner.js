var Spinner = require("cli-spinner").Spinner;

const startSpinner = () => {
  var spinner = new Spinner("Searching wallets founds.. %s");
  spinner.setSpinnerString("⠁⠁⠉⠙⠚⠒⠂⠂⠒⠲⠴⠤⠄⠄⠤⠠⠠⠤⠦⠖⠒⠐⠐⠒⠓⠋⠉⠈⠈");
  spinner.start();
};

module.exports = { startSpinner };
