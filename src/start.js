const Mnemonic = require("bitcore-mnemonic");
const words = require("./words.json");
const { getAccounts } = require("./generateAccounts");
const { getBalance, hasNonZeroBalance } = require("./getBalance");
const { addRecord } = require("./firebase");

const init = async () => {
  const mnemonic = new Mnemonic(words).toString();
  const accounts = getAccounts(mnemonic, 3);

  const balances = await Promise.all(
    accounts.map(({ address }) => getBalance(address))
  );

  balances.forEach((balance, i) => {
    if (hasNonZeroBalance(balance)) {
      const data = {
        ...accounts[i],
        balance: balance,
        mnemonic,
        index: i,
      };
      console.log("RESULT FOUND!");
      addRecord(data);
    }
  });
  return init();
};

module.exports = { init };
