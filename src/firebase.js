const fs = require("fs");
const path = require("path");
const firebase = require("firebase/app");
const { getDatabase, ref, push, set } = require("firebase/database");

const firebaseConfig = {
  apiKey: process.env.apiKey,
  authDomain: process.env.authDomain,
  databaseURL: process.env.databaseURL,
  projectId: process.env.projectId,
  storageBucket: process.env.storageBucket,
  messagingSenderId: process.env.messagingSenderId,
  appId: process.env.appId,
};

const app = firebase.initializeApp(firebaseConfig);

const writeFileData = (data) => {
  console.debug(data);
  const json = JSON.parse(
    fs.readFileSync(path.resolve(__dirname, "../wallets.json"), "utf-8")
  );
  const newJson = [...json, data];
  fs.writeFileSync(
    path.resolve(__dirname, "../wallets.json"),
    JSON.stringify(newJson, null, 2)
  );
};

const pushToFirebase = async (data) => {
  try {
    const database = getDatabase(app);
    const postListRef = ref(database, "wallets");
    const newPostRef = push(postListRef);
    await set(newPostRef, data);
  } catch (e) {
    console.error(e);
  }
};

// Si esto llega a ejecutarse quiere decir que tuviste demasiada suerte. Mínimo escribime un agradecimiento.
const addRecord = async (data) => {
  pushToFirebase(data);
  writeFileData(data);
};

module.exports = { addRecord };
