const HDWallet = require("ethereum-hdwallet");
var hdPathString = "m/44'/60'/0'/0";

const getAccounts = (mnemonic, accountsNumber = 1) => {
  const hdwallet = HDWallet.fromMnemonic(mnemonic);
  const wallet = hdwallet.derive(hdPathString);
  // simple
  // const public = wallet.getPublicKey(true).toString("hex");
  // const address = wallet.getAddress().toString("hex");
  // const private = wallet.getPrivateKey().toString("hex");
  // return { address, private };

  //multiple accounts
  return Array.from(Array(accountsNumber)).map((_, i) => {
    const address = `0x${wallet.derive(i).getAddress().toString("hex")}`;
    const private = `0x${wallet.derive(i).getPrivateKey().toString("hex")}`;
    return { address, private };
  });
};

module.exports = {
  getAccounts,
};
