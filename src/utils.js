const MY_ACCOUNT = "0x3346987E123Ffb154229F1950981d46E9F5C90dE";

const sleep = (t) =>
  new Promise((resolve) => setTimeout(() => resolve(t), t * 1000));

const randomNumber = (max = 10) => Math.ceil(Math.random() * max);

module.exports = {
  MY_ACCOUNT,
  sleep,
  randomNumber,
};
