const request = require("request");
const cheerio = require("cheerio");

const scrap = (address, baseUrl, cb) => {
  const url = `${baseUrl}${address}`;
  return new Promise((resolve, reject) => {
    request(url, function (error, _, body) {
      if (error || !body) {
        reject(error);
      }
      try {
        const $ = cheerio.load(body);
        return resolve(cb($));
      } catch (e) {
        return null;
      }
    });
  });
};

const getBalanceFromBlockchainCom = (address) =>
  scrap(address, "https://www.blockchain.com/eth/address/", ($) => {
    return $('span:contains("Final Balance")')
      .parent()
      .parent()
      .parent()
      .children('div:contains("ETH")')
      .text()
      .trim();
  });

const getBalanceFrometherchain = (address) =>
  scrap(address, "https://etherchain.org/account/", ($) => {
    return $("#pills-tabContent #overview")
      .find('div:contains("Balance:")')
      .text()
      .replace(/Balance:/gi, "")
      .trim();
  });

// not passing cloudfare ddos attak
// const getBalanceFromEtherScan = (address) =>
//   scrap(address, "https://etherscan.io/address/", ($) => {
//     return $("a#availableBalanceDropdown")
//       .first()
//       .text()
//       .trim()
//       .split("\n")
//       .find((s) => s.includes("$"));
//   });

const getBalance = async (address) => {
  try {
    const balance1 = await getBalanceFromBlockchainCom(address);
    if (!balance1) {
      const balance2 = await getBalanceFrometherchain(address);
      return balance2;
    }
    return balance1;
  } catch (e) {
    return null;
  }
};

const hasNonZeroBalance = (balance) => {
  return (
    balance &&
    !balance.includes("0.00000000 ETH") &&
    !balance.includes("0.00000 ETH")
  );
};

module.exports = { getBalance, hasNonZeroBalance };
