require("dotenv").config();
const { init } = require("./src/start");
const { startSpinner } = require("./src/spinner");

startSpinner();
init();
