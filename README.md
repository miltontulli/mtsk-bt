## Install

Be sure you are using correct node version listed in `./package.json` (`engines.node`)

`nvm use`

Install all dependencies with npm.

`npm install`

## Start

`npm start`
